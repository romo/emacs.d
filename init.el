;; -*- coding: utf-8; lexical-binding: t; -*-
(require 'cask "~/.cask/cask.el")
(cask-initialize)
(require 'org)
(org-babel-load-file "~/.emacs.d/emacs.org")
